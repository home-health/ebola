FROM \
    php:7.3.20-fpm-alpine
WORKDIR \
    /var/www
RUN \
    apk add \
        --no-cache \
        --update \
            composer \
            gnu-libiconv \
            util-linux \
    && \
    docker-php-ext-install \
        opcache \
        pdo_mysql \
    && \
    ln \
        -f \
        -s \
            "$PHP_INI_DIR/php.ini-production" \
            "$PHP_INI_DIR/php.ini" \
    && \
    rm \
        -r \
        -f \
            /var/cache/apk/* \
            /var/www/* \
    && \
    mkdir \
        /var/www/public
ENV \
    LD_PRELOAD \
    /usr/lib/preloadable_libiconv.so
COPY \
    /filesystem/ \
    /
CMD \
    start
