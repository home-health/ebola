# Ebola

Ebola is a Docker image based on Alpine Linux that contains Composer Dependency Manager and PHP FastCGI Process Manager
with extensions.

## Authentication

Authenticate to the GitLab Container Registry before use this image.

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

## Usage

Preferably, extend this image in a production environment.

```dockerfile
FROM registry.gitlab.com/home-health/ebola
COPY public public/
```

When running, this image listens for requests on `127.0.0.1:9000`.

## Contributing

See the [Contribution Guide](CONTRIBUTING.md).

## License

See the [End-User License Agreement (EULA)](LICENSE.md).
