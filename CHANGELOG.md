# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to
[Semantic Versioning](https://semver.org).

## [Unreleased]

## [1.3.8] - 2020-07-13

### Changed
- Docker image based on PHP 7.3.20 FPM.

## [1.3.7] - 2020-07-04

### Changed
- Specification of Docker 19.03.12 as a Continuous Integration image.
- Specification of Docker DinD 19.03.12 as a Continuous Integration service.

## [1.3.6] - 2020-06-14

### Changed
- Docker image based on PHP 7.3.19 FPM.

## [1.3.5] - 2020-06-02

### Changed
- Specification of Docker 19.03.11 as a Continuous Integration image.
- Specification of Docker DinD 19.03.11 as a Continuous Integration service.

## [1.3.4] - 2020-05-27

### Changed
- Specification of Docker 19.03.9 as a Continuous Integration image.
- Specification of Docker DinD 19.03.9 as a Continuous Integration service.
- Docker image based on PHP 7.3.18 FPM.

## [1.3.3] - 2020-05-03

### Changed
- Demonstrate image extension using relative path copy.

## [1.3.2] - 2020-04-17

### Changed
- Docker image based on PHP 7.3.17 FPM.

### Fixed
- Use the term FPM instead of CLI when referring to the PHP Docker image.

## [1.3.1] - 2020-03-27

### Changed
- Specification of Docker 19.03.8 as a Continuous Integration image.
- Specification of Docker DinD 19.03.8 as a Continuous Integration service.
- Docker image based on PHP 7.3.16 FPM.

## [1.3.0] - 2020-03-13

### Added
- Random collection of Linux utilities.

### Changed
- Reorder commands in test script alphabetically.

## [1.2.1] - 2020-03-11

### Changed
- Specification of Docker 19.03.7 as a Continuous Integration image.
- Specification of Docker DinD 19.03.7 as a Continuous Integration service.
- Docker image based on PHP 7.3.15 FPM.
- Use apostrophe character for strings.

## [1.2.0] - 2020-02-25

### Added
- GNU charset conversion library.

## [1.1.5] - 2020-02-23

### Changed
- Reorder attributes in Continuous Integration configuration file alphabetically.
- Reorder commands in test script alphabetically.

## [1.1.4] - 2020-02-22

### Changed
- Project description.

## [1.1.3] - 2020-02-21

### Changed
- Specification of Docker 19.03.5 as a Continuous Integration image.
- Specification of Docker DinD 19.03.5 as a Continuous Integration service.
- Limit 120 characters per line.
- Indent lines with 4 spaces.

## [1.1.2] - 2020-02-12

### Fixed
- Continuous Integration file reusability.
- Continuous Integration file verbosity.
- Continuous Integration file indenting.

### Removed
- Unnecessary script in Continuous Integration.

## [1.1.1] - 2020-02-11

### Changed
- Use parent folder of execution directory as workdir.

## [1.1.0] - 2020-02-11

### Changed
- Automatically manage execution directory permissions.

## [1.0.3] - 2020-02-10

### Changed
- Default execution directory.

## [1.0.2] - 2020-02-10

### Changed
- Docker image based on PHP 7.3.14 FPM.

## [1.0.1] - 2020-02-10

### Changed
- Test the PHP version.

## [1.0.0] - 2020-02-10

### Added
- Project readme.
- Production mode as default configuration.
- Continuous Integration.
- Test script.
- Remove unnecessary files from Docker build context.
- OPcache to compile and cache PHP scripts.
- Driver that implements PDO interface to enable access from PHP to MySQL.
- Command to clear installation cache.
- Composer as a package manager.
- Docker image based on Alpine Linux and PHP 7.3 FPM.
- Project license.
- Contributing guide.
- Record of all notable changes made to this project.
